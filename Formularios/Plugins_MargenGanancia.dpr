program Plugins_MargenGanancia;

uses
  Vcl.Forms,
  Plugins_MargenGanancia2 in 'Plugins_MargenGanancia2.pas' {Form1},
  UnitAppsComunes in '..\Unit\UnitAppsComunes.pas',
  UnitICG in '..\Unit\UnitICG.pas',
  UnitINI in '..\Unit\UnitINI.pas',
  UnitPluggins in '..\Unit\UnitPluggins.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
