unit Plugins_MargenGanancia2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  UnitAppsComunes,
  UnitICG,
  UnitPluggins,
  Data.DB,
  Data.Win.ADODB, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    BarraConexion: TStatusBar;
    Label3: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure consulta();
  private
    { Private declarations }
  public
    { Public declarations }
     pBdGeneral,
    pNombreEmpresa,
    pBdGestion: string;



    Cn001: TADOConnection;

    Rs001: TADOQuery;

    Ds001: TDataSource;

    Pr001: TParameter;

    //Variables
    ServerDB,
    NameDB,
    UsersDB,
    PasswordDB,
    StrSQL,
    Mensaje,
    ServerDB_C,
    NameDB_C :String;

    FSettings: TFormatSettings;

    Conectado, bGrabar :Boolean;

    Info_RegistryICG:TICG_DataRegistry;

    DatosPC: TDatosPc;

    Info_Pluggins: TICG_InfoPluggins;

    FLeerXMLPlugginFRT : TICG_XMLPlugginFRT;

   GetServer,
   GetDatabase,
   GetUsers,
   GetSerie,
   GetModoDocumento,
   GetTipoDocumento:String;
   GetNroDocumento,
   GetCodvendedor: Integer;


  end;

var
  Form1: TForm1;
   P1,P2,P3,P4: STRING;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
Form1.Close;
end;



 procedure tform1.consulta();
 begin
        StrSQL:='SELECT DESCRIPCION FROM  GESTION_NACHO.[dbo].VW_Plugin_MargenGanancia WHERE NUMSERIE = :PXMLN AND NUMFACTURA = :PXMLF ';


         Rs001:= TADOQuery.Create(Self);
         Rs001.Connection := cn001;
         Rs001.SQL.Add(StrSQL);
         {Parametros}
         Pr001 := Rs001.Parameters.ParamByName('PXMLN');
         Pr001.DataType := ftString;
         Pr001.Value := FLeerXMLPlugginFRT.Serie;
         Rs001.Prepared := true;

         Pr001 := Rs001.Parameters.ParamByName('PXMLF');
         Pr001.DataType := ftString;
         Pr001.Value := FLeerXMLPlugginFRT.NroDocumento;
         Rs001.Prepared := true;

         // ejecuta el query
           try
              Rs001.Open;
           except
           on e: exception do
              begin
                MessageDlg('Error: (No se encontro el Resultado) ' + e.Message, mtError,
                            [mbOK], 0);
                Exit;
              end;
           end;

         {Si es Verdadero Continuo}
         Rs001.Active;
         //se crea el Dataset
         Ds001         := TDataSource.Create(Self);
         Ds001.DataSet := Rs001;
         Ds001.Enabled := true;

        // EditCodigoUsuario.Text           :=Rs001.Fields[0].AsString;
         Label2.Caption      :=Rs001.Fields[0].AsString;
   end;{fin Datos del Cliente}

 procedure TForm1.FormCreate(Sender: TObject);
 var
  ruta : string;
 begin

    SySLocale.MiddleEast := True;
    // p1=usuario, p2=empresa gestion
    GetParametros(P1,P2,P3,P4);
    if P1=''  then P1:='0';
    if P2=''  then P2:='3';

    //busca los datos de la PC
    ObtenerDatosPC(DatosPC);
    {----------------------------------------------------------------------------------------------}
    {----------------------------------------------------------------------------------------------}
    //Obtengo Datos del XML
    //FLeerXMLPlugginFRT(LeerXMLPlugginFRT);
    ruta := ExtractFileDir(Application.ExeName) + '\' + 'icgdo_pl_cierredocumento.xml';
    //eti.Caption := 'Se Crear� el Archivo en... ' + Ruta;
    LeerXMLPlugginFRT(ruta,FLeerXMLPlugginFRT);
    GetServer       :=  FLeerXMLPlugginFRT.Server;
    GetDatabase     :=  FLeerXMLPlugginFRT.Database;
    GetUsers        :=  FLeerXMLPlugginFRT.Users;
    GetSerie        :=  FLeerXMLPlugginFRT.Serie;
    GetModoDocumento:=  FLeerXMLPlugginFRT.ModoDocumento;
    GetTipoDocumento:=  FLeerXMLPlugginFRT.TipoDocumento;
    GetNroDocumento :=  FLeerXMLPlugginFRT.NroDocumento;
    GetCodvendedor  :=  FLeerXMLPlugginFRT.Codvendedor;

    Info_RegistryICG.NroEmpresaGestion:= StrToInt(P2);
    UsersDB:='ICGAdmin';
    PasswordDB:='';
    Conectado:=FALSE;

    Get_password_ICGAdmin(CONECTADO, 'Software\ICG\Plugins_Curso', Info_Pluggins );

      if Info_Pluggins.PassworDB='' then   PasswordDB:='masterkey' else PasswordDB:=Info_Pluggins.PassworDB;
       Datos_ICGRegistry_general(Info_RegistryICG,4);
       ServerDB:=Info_RegistryICG.ServerNameGeneral;
       NameDB:= Info_RegistryICG.DBGeneral;
        if (ServerDB='' ) or (NameDB='') then
          begin
           ShowMessage('Error: No se ha podido optener los datos del servidor y/o Base de datos GENERAL en el registro del equipo.'  ) ;
          CloseApp;
          Exit();
        end;
       Cn001  := TADOConnection.Create(nil);
       //Rs001    := TADOQuery.Create(nil);

       Conectado:=Open_DB (Cn001 , 2 , ServerDB, NameDB, UsersDB, PasswordDB, '' );
      //si no hay conexion
      if not Conectado then
        Begin
          ShowMessage('Error: No se ha podido conectarse a la Base de datos: ' + NameDB + ' Servidor SQL: ' +ServerDB ) ;

          Conectado:=TRUE;
          Get_password_ICGAdmin(CONECTADO, 'Software\ICG\ICG_plugins', Info_Pluggins );

          if  Info_Pluggins.PassworDB<>'' then
            begin
              ShowMessage('Se ha guardado un Password Nuevo para el usuario ICGAdmin, se va a proceder a cerrar la aplicaci�n.'  ) ;
              ShowMessage('Debe ejecutar de nuevo la aplicaci�n para poder validar los datos.'  ) ;

            end;

            CloseApp;
            Exit();
      end;
      Cn001.Close;
      Cn001.Destroy;
      Datos_ICGRegistry(Info_RegistryICG,Info_RegistryICG.tipofront,DatosPC,UsersDB,PasswordDB) ;

      ServerDB:=Info_RegistryICG.ServerNameGestion;
      if (ServerDB='' ) or (NameDB='') then
        begin
          ShowMessage('Error: No se ha podido optener los datos del servidor y/o Base de datos de Gesti�n.'  ) ;
          CloseApp;
          Exit();
        end;

        Cn001    := TADOConnection.Create(nil);
        Conectado:= Open_DB (Cn001 , 2 , ServerDB, NameDB, UsersDB, PasswordDB, '' );
        if NOT CONECTADO then
        begin
            ShowMessage('Error: No se ha podido abrir la Base de datos o El servidor no responde'  ) ;
            exit;
        end;
         consulta();
         BarraConexion.Panels[0].Text:='Servidor: ' +ServerDB ;
  end;//fin de proceso

end.
