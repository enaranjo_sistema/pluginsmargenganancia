//******************************************************************************
// Unit que contiene los procedimientos y funciones de uso Comun dentro del
// Programa
// Empresas: Consultoria & Sistemas ICG, C.A. / Geinfor Venezuela, C.A.
// Autor: Rafael Rangel
// Fecha: 31/03/2013
// Ultima Modificacion: 09-06-2015
//******************************************************************************
unit UnitAppsComunes;

interface


Uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics,  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls, Vcl.Buttons,IniFiles, ADODB ,Data.DB,IdBaseComponent,
  IdComponent, IdRawBase, IdRawClient, IdIcmpClient,WinInet, WinSock,Registry,
  Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc  ;

//Tipos de Registros
type TDatosPC   = record
  Nombre, IP, Usuario :String;
end;




Var
  IniFile : TIniFile;
  //Funciones-------------------------------------------------------------------
  //Funcion Para Abrir una Conexion a una Base de datos
  function Open_DB (CnADO :TADOConnection ; OpcionBd:Integer ; Servidor, BD, Usuario, Clave, Ruta : String):Boolean;
  //Funcion para abrir un Data Set
  Function Open_ADO_Qry(CnADO:TADOConnection; RsDataSet:TADOQuery ;StrSql:String;Lectura:Boolean): Boolean;
  //Funcion Para Crear una Base de datos
  Function DB_Function(CnADO:TADOConnection;SQL_Function:string;Opcion_Function:Integer): Boolean;
  //Funcion para Hacer in Ping a un PC
  Function Ping_PC(IdCliente:TIdIcmpClient;PC:string): Boolean ;
  //Funcion para Verificar si existe una Llave en el Registro
  Function ExisteKey_Registry (Root_Key:HKEY;App_Key :String; lmensaje:Boolean):Boolean;
  //Funcion para tomar valores del registro de Tipo String o Integer
  function GetRegistryData(RootKey: HKEY; Key, Value: string): variant;
  //Funcion para Ejecutar programas externos
  function EjecutarPrograma(sPrograma: String; Visibilidad: Integer ): Integer;
  //Funcion para rellenar con ceros a la izquierda
  Function FillSpaces(cVar:String;nLen:Integer):String;
  //Funcion para convertir numeros a letras
  function NumToLetra(const mNum: Currency; const iIdioma, iModo: Smallint): String;
  //Funcion para utilizar la Impresora Fiscal
  function ImpFiscal_Bixolon (var pRuta:String;pInfoSalida:Pointer):Integer; stdcall external 'IF_Bixolon.dll'  name 'ImprimirVoucher';
  //Funcion para Validar el RIF o Cedula del cliente
  Function ValidarCIF (CIF :String):Boolean;
  //Funcion para verificar si el valor es numerico
  function esNumero (valor : string) : boolean;
  //Funcion para obtener informacion de acuerdo al tipo de front
  function InfoTipoFront (TipoFront,CualInfo:Integer) : String;
  //Funcion para Encriptar una cadena
  function Encrypt(tr: string): string;
  //function descifrar(cadena, Key: string): String;
  function Decrypt(pr: string): string;
  //Funcion para Obtener la version de la aplicacion
  function Version_Info: String;
  //Funcion Para Crear la estruct del XML
  function MakeXmlStr (node, value: string): string  ;
  //Funcion Para Crear los Campos del XML
  function FieldsToXml (rootName: string; data: TADOQuery): string;
  //Funcion Para obtener las fechas de un archivo
  function GetFileTimes(FileName : string;var Created : TDateTime;var Modified : TDateTime;  var Accessed : TDateTime) : boolean;
  //separa cadenas segun el separador
  Function SepararCadena(const Cadena: string; const Delim: Char): TStringList;
  //Convierte un numero en texto y en formato ####.## para grabar en sql
  Function Formato(Pnumero: Double) : string;
  //convierte un numero string de  ####,##  a  ####.##
  Function FormatoStr(Pnumero :string) : string;

  //Funcion para Verificar q existe una tabla
  Function Hay_table(CnADO:TADOConnection ; TABLE :String):Boolean;

  //Optiene los parametros de la tabla Z_IVAPARAMETROS
  function get_BuscaParametros(pClave, pSubClave, pUsuario: String ; CnADO: TADOConnection):string;
  //Verifica que sea fecha
  function esFecha (valor : string) : boolean;
  //retorna ul ultimo dia del mes
  function LastDay(const pFecha: TDateTime): TDateTime;
  //retorna caracteres a la derecha
  function RightStr2(const s:string;const count:integer):String;


  //----------------------------------------------------------------------------
  //Procedimientos--------------------------------------------------------------
  //----------------------------------------------------------------------------
  //Procedimiento Para Cerra la Conexion a la base de datos
  Procedure CloseConnection(CnADO:TADOConnection; RsDataSet:TADOQuery);
  //Procedimiento para cerrar las aplicaciones
  Procedure Close_EXE (Apps:String);
  //Procedimiento para Obtener informcion del equipo
  procedure ObtenerDatosPC (var Datos:TDatosPC );
  //Procedimiento para escribir valores en el Registro de Windows---------------
  procedure SetRegistryData(RootKey: HKEY; Key, Value: string; RegDataType: Integer; Data: variant);
  //Definicion de Procedimiento que Cierra la aplicacion activa
  Procedure CloseApp;
  //Definicion del procedimiento para borrar Archivos
  procedure EliminarArchivo (sArchivo: String);
  //Procedimiento Para Obtener la Version de la Aplicacion
  procedure GetBuild_Info(var V1, V2, V3, V4: Word);
  // Generar un arhivo texto que funcione como un archivo log
  procedure BitacoraTXT(Archivo,Contenido:String);
  //lee parametros  pasados a la aplicaci�n
  procedure GetParametros(var P1,P2,P3,P4:String);

  //proceidmiento para dar formateo a los campos numericos
  procedure FormateaCampos(var query: TADOQuery ;  MisCamposFormateados, ptipo: string );

  //GUARDA PARAMETROS EN EL MODULO
  Procedure Save_Parametros(pClave, pSubClave, pUsuario,pParametro: String ; CnADO: TADOConnection);



implementation
//******************************************************************************
//Funciones
//******************************************************************************


//retorna el ultimo dia del mes
function LastDay(const pFecha: TDateTime): TDateTime;
var
  viDia, viMes, viAno: word;
begin

  DecodeDate(pFecha, viAno, viMes, viDia);

  //Avanzamos un mes.
  viMes := viMes + 1;

  //Si nos hemos pasado, avanzamos el a�o.
  if viMes = 13 then begin

  viMes := 1;
  viAno := viAno + 1;

  end;//if viMes = 13

  //Y devolvemos el d�a anterior al primer d�a del mes siguiente.
  result := EncodeDate(viAno, viMes, 1) - 1;

end;//LastDay

//------------------------------------------------------------------------------
//Funcion Para Abrir la Base de datos utilizando Ado y varios Provedores de base de datos
//------------------------------------------------------------------------------
Function Open_DB (CnADO :TADOConnection ; OpcionBd:Integer ; Servidor, BD, Usuario, Clave, Ruta: String ): Boolean;

//Variables de la Funcion
Var
  ConStr: String; // Cadena de Conexion

Begin

  //Cerramos la Conexion
  if CnADO.Connected then CnADO.close ;
  Case OpcionBd  of
    1 :  //'Excel
      Begin
        //Se Arma la cadena de Conexion
        ConStr := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + Ruta + BD + ';Persist Security Info=False; Extended Properties=Excel 8.0;';

      End;

    2 : //SQL Server
      Begin
        // Se Arma La Cadena de Conexion
        ConStr:=  'Provider=sqloledb;' +
                  'Data Source=' + Servidor + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'User id=' + Usuario + ';' +
                  'Password=' + Clave + ';' ;


      End;

      3: // Access
      Begin
        ConStr:= 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                  'Data Source=' + Ruta + Bd + '.mdb' +
                  ';Persist Security Info=False ' +  ';' +
                  'Jet OLEDB:Database Password=' + Clave ;

      End;

      4: //Oracle
      Begin
        ConStr:= 'Provider=msdaora;' +
                  'Data Source=' + Servidor + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'User id=' + Usuario + ';' +
                  'Password=' + Clave + ';' ;
      End;
      5: // ODBC
      Begin
        ConStr:= 'DSN=' + Bd +  ';pwd=' + Clave ;

      End;
      6:// Texto
      Begin
        ConStr:='DRIVER={Microsoft Text Driver (*.txt; *.csv)};' +
                'DBQ=' +  Ruta + ';';
      End;

      7:// MySql
      Begin

      End;

      8: // dBase
      Begin
        ConStr:= 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                  'Data Source=' + Ruta + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'Extended Properties=dBASE IV;'

      End;
      9: // Paradox
      Begin
        ConStr:= 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                  'Data Source=' + Ruta + ';' +
                  'Initial Catalog=' + BD + ';' +
                  'Extended Properties=Paradox 5.x;'
      End;

      10: // SQL Lite
      Begin
       ConStr:= 'Provider=SQLITEDB;' +
                  'Data Source=' + Ruta + Bd + '.db' ;
                 // ';Persist Security Info=False ' +  ';' +
                 // 'Jet OLEDB:Database Password=' + Clave ;


      End;




  end;


  Result:=false;
  CnADO.Close ;
  CnADO.ConnectionString:=ConStr  ;
  CnADO.LoginPrompt:=false;


  if (NOT CnADO.Connected) Then
  Try
    //Se Abre la Conexion a la Base de datos
    //CnADO.Mode:=cmRead;
    CnADO.Open;

    Result:=True;

  // Si no se puede conectar se devuelve falso y el mensaje de error
  Except On E:Exception do
    Begin
        MessageDlg('No se Puede Conectar a ' + Servidor + ':' + BD + ' El error es : ' + #13#10 + E.Message,
        mterror, [mbok],0);
        Result:=False;
    End;
  End;

End;
//------------------------------------------------------------------------------

//Verifica si existe una tabla dentro de la BD
 Function Hay_table(CnADO:TADOConnection ; TABLE :String):Boolean;
 var

  _tmp : tstringlist;
    i : integer;
 BEGIN
  Result:=False;
   _tmp := tstringlist.create;
   _tmp.Clear;

  if CnADO.Connected Then
  Begin
     CnADO.GetTableNames( _tmp,False);
      if _tmp.Count > 0 then
      begin
        for i := 0 to _tmp.Count - 1 do
        begin
         if _tmp[i]=table then  Result:=true;
        end;
      end;

  end;
 END;



//Funcion que Abre un DataSet
Function Open_ADO_Qry(CnADO:TADOConnection; RsDataSet:TADOQuery ;StrSql:String;Lectura:Boolean): Boolean;
Begin
  Result:=False;

  if CnADO.Connected Then
  Begin
    with RsDataSet do begin
      Close;
      Connection:=CnADO;
      SQL.Clear;
      SQL.Text:=StrSQL;
      //LockType:=ltReadOnly;

      // Verificamos que se pueda abrir la Consulta
      try
        Open;
        Result:=True;

      Except On E:Exception do
        Begin
          MessageDlg('No se Puede Consultar la Informacion Solicitada, el Error es :' + E.Message,
          mterror, [mbok],0);
          Result:=False;
        End;
      end;
    End;
  End;
End ;


//Funcion para realizar diferentes acciones en la base de datos-----------------
Function DB_Function(CnADO:TADOConnection;SQL_Function:string;Opcion_Function:Integer): Boolean;
Var
  LaAccion: String; // Cadena de Conexion
begin

  Result:=False;
  if CnADO.Connected Then ; // Si la conexion esta Abierta
  Begin
    try

      LaAccion:=' ';
      Case Opcion_Function of
        1 :  //Crear una Base de datos
        Begin
          LaAccion:='CREATE DATABASE IF NOT EXISTS ';
        End;
        2 :  //Crear una Tabla
        Begin
          LaAccion:='CREATE TABLE  ';
        End;

        3 :  //Borrar una Tabla
        Begin
          LaAccion:='DROP TABLE IF EXISTS ';
        End;

        4 :  //Ejecutar SQL
        Begin
          LaAccion:=' '
        End;
      End;

      CnADO.Execute(LaAccion + SQL_Function,cmdText);
      Result:=True;
      Except On E:Exception do
      Begin
        MessageDlg('No se Puede Ejecutar la acci�n :'  +  LaAccion + SQL_Function + ' , el Error es :' + E.Message,
        mterror, [mbok],0);
      End;
    end
  End  ;
end;
//------------------------------------------------------------------------------


//Funcion Para realizarle un Ping a un PC y saber si esta en la red-------------
Function Ping_PC(IdCliente:TIdIcmpClient;PC:string): Boolean ;
begin
  IdCliente.Host:=PC;
  IdCliente.ReceiveTimeout:=2000;
  Result:=False;
  // Se Ejecuta el PINg
  try
    IdCliente.Ping('Prueba de IP correcta',0);
    Result:=True;
    Except On E:Exception do
    Begin
        MessageDlg('El Equipo : '  +  PC + ' No esta en la red o no esta Activo, el Error es :' + E.Message,
        mterror, [mbok],0);
         Result:=False;
    End;
  end;
end;
//------------------------------------------------------------------------------


//Fuction par retornar caracteres a la derecha
function RightStr2(const s:string;const count:integer):String;
begin
   Result := Copy(s, Length(s)+1- count, count);
end;




//Funcion para Verificar si existe una clave en el registro---------------------
Function ExisteKey_Registry (Root_Key: HKEY;App_Key:String;lMensaje:Boolean  ):Boolean;
var
  reg : TRegistry;
begin
  reg := TRegistry.Create(KEY_READ);
  Result:=False;
  reg.RootKey := Root_Key;
  if (not reg.KeyExists(App_Key)) then
    begin
      if lMensaje then
        MessageDlg('No se encontro en el Registro la llave: ' + App_Key,mterror, [mbok],0);

      Result:=False
    end
    Else
    Begin
      Result:=True;
    End;
  //Cerramos el Registro
  reg.CloseKey();
  reg.Free;
end;
//------------------------------------------------------------------------------


//Funcion para Leer Valores del Registro----------------------------------------
function GetRegistryData(RootKey: HKEY; Key, Value: string): variant;
var
  Reg: TRegistry;
  RegDataType: TRegDataType;
  DataSize, Len: integer;
  s: string;
label cantread;
begin
  Reg := nil;
  try
    Reg := TRegistry.Create(KEY_QUERY_VALUE);
    Reg.RootKey := RootKey;
    if Reg.OpenKeyReadOnly(Key) then begin
      try
        RegDataType := Reg.GetDataType(Value);
        if (RegDataType = rdString) or
           (RegDataType = rdExpandString) then
          Result := Reg.ReadString(Value)
        else if RegDataType = rdInteger then
          Result := Reg.ReadInteger(Value)
        else if RegDataType = rdBinary then begin
          DataSize := Reg.GetDataSize(Value);
          if DataSize = -1 then goto cantread;
          SetLength(s, DataSize);
          Len := Reg.ReadBinaryData(Value, PChar(s)^, DataSize);
          if Len <> DataSize then goto cantread;
          Result := s;
        end else
cantread:
      raise Exception.Create(SysErrorMessage(ERROR_CANTREAD));
      except
        s := ''; // Deallocates memory if allocated
        Reg.CloseKey;
        raise;
      end;
      Reg.CloseKey;
    end else
    //    raise Exception.Create(SysErrorMessage(GetLastError));
    // Si se ubica el valor en el registro se detiene la ejecucion del programa
    raise Exception.Create('No se ubico el Valor en el Registro ' + Key + ' No se puede CONTINUAR con la Ejecucion del Programa');

  except
    Reg.Free;
    raise;
  end;
  Reg.Free;
end;
//------------------------------------------------------------------------------


//Funcion Para Ejecutar programa Externo y esperar que termine
function EjecutarPrograma( sPrograma: String; Visibilidad: Integer ): Integer;
var
  sAplicacion: array[0..512] of char;
  DirectorioActual: array[0..255] of char;
  DirectorioTrabajo: String;
  InformacionInicial: TStartupInfo;
  InformacionProceso: TProcessInformation;
  iResultado, iCodigoSalida: DWord;
begin
  StrPCopy( sAplicacion, sPrograma );
  GetDir( 0, DirectorioTrabajo );
  StrPCopy( DirectorioActual, DirectorioTrabajo );
  FillChar( InformacionInicial, Sizeof( InformacionInicial ), #0 );
  InformacionInicial.cb := Sizeof( InformacionInicial );

  InformacionInicial.dwFlags := STARTF_USESHOWWINDOW;
  InformacionInicial.wShowWindow := Visibilidad;
  CreateProcess( nil, sAplicacion, nil, nil, False,
                 CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS,
                 nil, nil, InformacionInicial, InformacionProceso );

  // Espera hasta que termina la ejecuci�n
  repeat
    iCodigoSalida := WaitForSingleObject( InformacionProceso.hProcess, 1000 );
    Application.ProcessMessages;
  until ( iCodigoSalida <> WAIT_TIMEOUT );

  GetExitCodeProcess( InformacionProceso.hProcess, iResultado );
  MessageBeep( 0 );
  CloseHandle( InformacionProceso.hProcess );
  Result := iResultado;
end;
//------------------------------------------------------------------------------

//Funcion para rellenar con ceros a la izquierda campos string
Function FillSpaces(cVar:String;nLen:Integer):String;
 begin
 Result:=StringOfChar('0',nLen - Length(cVar))+cVar;
 end;
//------------------------------------------------------------------------------



// Funcion para convertir numeros a Letras
function NumToLetra(const mNum: Currency; const iIdioma, iModo: Smallint): String;
 const
   iTopFil: Smallint = 6;
   iTopCol: Smallint = 10;
   aCastellano: array[0..5, 0..9] of PChar =
   ( ('UNA ','DOS ','TRES ','CUATRO ','CINCO ',
     'SEIS ','SIETE ','OCHO ','NUEVE ','UN '),
     ('ONCE ','DOCE ','TRECE ','CATORCE ','QUINCE ',
     'DIECISEIS ','DIECISIETE ','DIECIOCHO ','DIECINUEVE ',''),
     ('DIEZ ','VEINTE ','TREINTA ','CUARENTA ','CINCUENTA ',
     'SESENTA ','SETENTA ','OCHENTA ','NOVENTA ','VEINTI'),
     ('CIEN ','DOSCIENTAS ','TRESCIENTAS ','CUATROCIENTAS ','QUINIENTAS ',
     'SEISCIENTAS ','SETECIENTAS ','OCHOCIENTAS ','NOVECIENTAS ','CIENTO '),
     ('CIEN ','DOSCIENTOS ','TRESCIENTOS ','CUATROCIENTOS ','QUINIENTOS ',
     'SEISCIENTOS ','SETECIENTOS ','OCHOCIENTOS ','NOVECIENTOS ','CIENTO '),
     ('MIL ','MILLON ','MILLONES ','CERO ','Y ',
     'UNO ','DOS ','CON ','','') );
   aCatalan: array[0..5, 0..9] of PChar =
   ( ( 'UNA ','DUES ','TRES ','QUATRE ','CINC ',
     'SIS ','SET ','VUIT ','NOU ','UN '),
     ( 'ONZE ','DOTZE ','TRETZE ','CATORZE ','QUINZE ',
     'SETZE ','DISSET ','DIVUIT ','DINOU ',''),
     ( 'DEU ','VINT ','TRENTA ','QUARANTA ','CINQUANTA ',
     'SEIXANTA ','SETANTA ','VUITANTA ','NORANTA ','VINT-I-'),
     ( 'CENT ','DOS-CENTES ','TRES-CENTES ','QUATRE-CENTES ','CINC-CENTES ',
     'SIS-CENTES ','SET-CENTES ','VUIT-CENTES ','NOU-CENTES ','CENT '),
     ( 'CENT ','DOS-CENTS ','TRES-CENTS ','QUATRE-CENTS ','CINC-CENTS ',
     'SIS-CENTS ','SET-CENTS ','VUIT-CENTS ','NOU-CENTS ','CENT '),
     ( 'MIL ','MILIO ','MILIONS ','ZERO ','-',
     'UN ','DOS ','AMB ','','') );
 var
   aTexto: array[0..5, 0..9] of PChar;
   cTexto, cNumero: String;
   iCentimos, iPos: Smallint;
   bHayCentimos, bHaySigni: Boolean;

   (*************************************)
   (* Cargar Textos seg�n Idioma / Modo *)
   (*************************************)

   procedure NumLetra_CarTxt;
   var
     i, j: Smallint;
   begin
     (* Asignaci�n seg�n Idioma *)

     for i := 0 to iTopFil - 1 do
       for j := 0 to iTopCol - 1 do
         case iIdioma of
           1: aTexto[i, j] := aCastellano[i, j];
           2: aTexto[i, j] := aCatalan[i, j];
         else
           aTexto[i, j] := aCastellano[i, j];
         end;

     (* Asignaci�n si Modo Masculino *)

     if (iModo = 1) then
     begin
       for j := 0 to 1 do
         aTexto[0, j] := aTexto[5, j + 5];

       for j := 0 to 9 do
         aTexto[3, j] := aTexto[4, j];
     end;
   end;

   (****************************)
   (* Traducir D��gito -Unidad- *)
   (****************************)

   procedure NumLetra_Unidad;
   begin
     if not( (cNumero[iPos] = '0') or (cNumero[iPos - 1] = '1')
      or ((Copy(cNumero, iPos - 2, 3) = '001') and ((iPos = 3) or (iPos = 9))) ) then
       if (cNumero[iPos] = '1') and (iPos <= 6) then
         cTexto := cTexto + aTexto[0, 9]
       else
         cTexto := cTexto + aTexto[0, StrToInt(cNumero[iPos]) - 1];

     if ((iPos = 3) or (iPos = 9)) and (Copy(cNumero, iPos - 2, 3) <> '000') then
       cTexto := cTexto + aTexto[5, 0];

     if (iPos = 6) then
       if (Copy(cNumero, 1, 6) = '000001') then
         cTexto := cTexto + aTexto[5, 1]
       else
         cTexto := cTexto + aTexto[5, 2];
   end;

   (****************************)
   (* Traducir D��gito -Decena- *)
   (****************************)

   procedure NumLetra_Decena;
   begin
     if (cNumero[iPos] = '0') then
       Exit
     else if (cNumero[iPos + 1] = '0') then
       cTexto := cTexto + aTexto[2, StrToInt(cNumero[iPos]) - 1]
     else if (cNumero[iPos] = '1') then
       cTexto := cTexto + aTexto[1, StrToInt(cNumero[iPos + 1]) - 1]
     else if (cNumero[iPos] = '2') then
       cTexto := cTexto + aTexto[2, 9]
     else
       cTexto := cTexto + aTexto[2, StrToInt(cNumero[iPos]) - 1]
         + aTexto[5, 4];
   end;

   (*****************************)
   (* Traducir D��gito -Centena- *)
   (*****************************)

   procedure NumLetra_Centena;
   var
     iPos2: Smallint;
   begin
     if (cNumero[iPos] = '0') then
       Exit;

     iPos2 := 4 - Ord(iPos > 6);

     if (cNumero[iPos] = '1') and (Copy(cNumero, iPos + 1, 2) <> '00') then
       cTexto := cTexto + aTexto[iPos2, 9]
     else
       cTexto := cTexto + aTexto[iPos2, StrToInt(cNumero[iPos]) - 1];
   end;

   (**************************************)
   (* Eliminar Blancos previos a guiones *)
   (**************************************)

   procedure NumLetra_BorBla;
   var
     i: Smallint;
   begin
     i := Pos(' -', cTexto);

     while (i > 0) do
     begin
       Delete(cTexto, i, 1);
       i := Pos(' -', cTexto);
     end;
   end;

 begin
   (* Control de Argumentos *)

   if (mNum < 0.00) or (mNum > 999999999999.99) or (iIdioma < 1) or (iIdioma > 2)
     or (iModo < 1) or (iModo > 2) then
   begin
     Result := 'ERROR EN ARGUMENTOS';
     Abort;
   end;

   (* Cargar Textos seg�n Idioma / Modo *)

   NumLetra_CarTxt;

   (* Bucle Exterior -Tratamiento C�ntimos-     *)
   (* NOTA: Se redondea a dos d��gitos decimales *)

   cNumero := Trim(Format('%12.0f', [Int(mNum)]));
   cNumero := StringOfChar('0', 12 - Length(cNumero)) + cNumero;
   iCentimos := Trunc((Frac(mNum) * 100) + 0.5);

   repeat
     (* Detectar existencia de C�ntimos *)

     if (iCentimos <> 0) then
       bHayCentimos := True
     else
       bHayCentimos := False;

     (* Bucle Interior -Traducci�n- *)

     bHaySigni := False;

     for iPos := 1 to 12 do
     begin
       (* Control existencia D��gito significativo *)

       if not(bHaySigni) and (cNumero[iPos] = '0') then
         Continue
       else
         bHaySigni := True;

       (* Detectar Tipo de D��gito *)

       case ((iPos - 1) mod 3) of
         0: NumLetra_Centena;
         1: NumLetra_Decena;
         2: NumLetra_Unidad;
       end;
     end;

     (* Detectar caso 0 *)

     if (cTexto = '') then
       cTexto := aTexto[5, 3];

     (* Traducir C�ntimos -si procede- *)

     if (iCentimos <> 0) then
     begin
       cTexto := cTexto + aTexto[5, 7];
       cNumero := Trim(Format('%.12d', [iCentimos]));
       iCentimos := 0;
     end;
   until not (bHayCentimos);

   (* Eliminar Blancos innecesarios -s�lo Catal�n- *)

   if (iIdioma = 2) then
     NumLetra_BorBla;

   (* Retornar Resultado *)

   Result := Trim(cTexto);
 end;

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Funcion para Validar el RIF o la Cedula de Identidad

//Funcion para Verificar si existe una clave en el registro---------------------
Function ValidarCIF (CIF :String):Boolean;
Var

LargoCIF:Integer;
TipoCIF,
RestoCIF,
Mensaje:String;
ElResultado:Boolean;
begin

  LargoCIF:=Length(CIF);
  ElResultado:=True;
  if LargoCIF > 1 then
  Begin
    TipoCIF:=(Copy(CIF,1,1));
    RestoCIF:=(Copy(CIF,2,LargoCIF));
    //Se Compara el primer Digito de la cedula o RIF del cliente
    if (TipoCIF <> 'V') And (TipoCIF <> 'E') And (TipoCIF <> 'J')And (TipoCIF <> 'G') then
    Begin
      Mensaje:='El RIF/CI no cumple con el formato Requerido, Solo se Aceptan V,J,G,E Usted Indico: ' + CIF;
      ElResultado:=False ;
    End
    else
    Begin
      ElResultado:=esNumero (RestoCIF);
      Mensaje:='El RIF/CI no cumple con el formato Requerido, Solo se Aceptan Numeros, Usted Indico: ' + RestoCIF;
     // ElResultado:=True ;
    End;
  End
  Else
  Begin
    Mensaje:='El RIF/CI no cumple con el formato Requerido, la longitud es muy corta: ' + CIF;
    ElResultado:=False ;
  End;

  //Si no cumple con las condiciones se muestra el Mensaje
  if ElResultado=False then MessageDlg(Mensaje,mterror, [mbok],0);
  Result:=ElResultado;

end;
//------------------------------------------------------------------------------


//Comprueba si un valor es num�rico
function esNumero (valor : string) : boolean;
var
  numero : integer;
begin
  try
    numero := strtoint(valor);
    result := true;
  except
    result := false;
  end;
end;


//Comprueba si un valor es num�rico
function esFecha (valor : string) : boolean;
var
  resultado : Tdatetime;
begin
  try
    resultado := strtodatetime(valor);
    result := true;
  except
    result := false;
  end;
end;
//------------------------------------------------------------------------------

//Devuelve informacion de acuerdo al tipo de Front
function InfoTipoFront (TipoFront,CualInfo:Integer) : String;
var
  Info : String;
begin
  Info:='';
  //Segun el tipo de Front la aplicacion se ajusta a la estructura de datos
  Case TipoFront  of
    1: //FrontRest
    Begin

      Case CualInfo  of
        1: // SQL para Buscar un cliente
        Begin
          Info:='';
          Info:='SELECT CODCLIENTE,CIF, CODCONTABLE, NOMBRECOMERCIAL, DIRECCION1, TELEFONO1, E_MAIL, '  +
          ' NOMBRECLIENTE, DESCATALOGADO' +
          ' FROM CLIENTES WHERE DESCATALOGADO <> ''T''';
        End;

        2: // SQL para Buscar los Rangos de Clientes
        Begin
          Info:='';
          Info:='SELECT  PuedeCrear, Minimo, Maximo FROM REM_RANGOS' +
              ' WHERE (IDFront = 0) AND (Tipo = 2)' ;
        End;

        3: // SQL para Buscar la informacion de los pluggins
        Begin
          Info:='';
          Info:='SELECT FILENAMEXML FROM PLUGGINS ' ;
        End;
        4: // SQL para Buscar la informacion del UltimoCliente
        Begin
          Info:='';
          Info:='SELECT MAX(CODCLIENTE) AS ULTIMOCLIENTE FROM CLIENTES ';
        End;

      End;
    End;
    2 : //FronRetail
    Begin
      //Se Define que se Buscara en el registro de Windows
      Info:='';
      Case CualInfo  of
        1: // SQL para Buscar un cliente
        Begin
          Info:='';
          Info:='SELECT CODCLIENTE,CIF, CODCONTABLE, NOMBRECOMERCIAL, DIRECCION1, TELEFONO1, E_MAIL, '  +
          ' NOMBRECLIENTE, DESCATALOGADO' +
          ' FROM CLIENTES WHERE DESCATALOGADO <> ''T''';
        End;

        2: // SQL para Buscar los Rangos de Clientes
        Begin
          Info:='';
          Info:='SELECT  PuedeCrear, Minimo, Maximo FROM REM_RANGOS' +
              ' WHERE (IDFront = 0) AND (Tipo = 2)' ;
        End;

        3: // SQL para Buscar la informacion de los pluggins
        Begin
          Info:='';
          Info:='SELECT FILENAMEXML FROM PLUGGINS ' ;
        End;
        4: // SQL para Buscar la informacion del UltimoCliente
        Begin
          Info:='';
          Info:='SELECT MAX(CODCLIENTE) AS ULTIMOCLIENTE FROM CLIENTES ';
        End;

      End;

    End;
    3 : //Front Hotel
    Begin
      //Se Define que se Buscara en el registro de Windows
      Info:='';
    End;

     4 : //Manager
    Begin
      Case CualInfo  of
        1: // SQL para Buscar un cliente
        Begin
          Info:='';
          Info:='SELECT CODCLIENTE,CIF, CODCONTABLE, NOMBRECOMERCIAL, DIRECCION1, TELEFONO1, E_MAIL, '  +
          ' NOMBRECLIENTE, DESCATALOGADO' +
          ' FROM CLIENTES WHERE DESCATALOGADO <> ''T''';
        End;

        2: // SQL para Buscar los Rangos de Clientes
        Begin
          Info:='';
          Info:='SELECT  PuedeCrear, Minimo, Maximo FROM REM_RANGOS' +
              ' WHERE (IDFront = 0) AND (Tipo = 2)' ;
        End;

        3: // SQL para Buscar la informacion de los pluggins
        Begin
          Info:='';
          Info:='SELECT FILENAMEXML FROM PLUGGINS ' ;
        End;
        4: // SQL para Buscar la informacion del UltimoCliente
        Begin
          Info:='';
          Info:='SELECT MAX(CODCLIENTE) AS ULTIMOCLIENTE FROM CLIENTES ';
        End;

      End;
    End;
  End;


  Result:= Info;
end;

//----------------------------------------------------------------------------
// Cifrar una cadena
//----------------------------------------------------------------------------

//Encrypt
function Encrypt(tr: string): string;
var
  i: Integer;
  Temp: string;

begin
  for i := 1 to Length(tr) do
  begin
    Temp := Temp + Chr(Ord(tr[i]) + 1);
  end;
  Encrypt := Temp;
end;
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//Descifrar una Cadena
//----------------------------------------------------------------------------

//Decrypt
function Decrypt(pr: string): string;
var
  i: Integer;
  Temp: string;
  begin
  for i := 1 to Length(pr) do
  begin
    Temp := Temp + Chr(Ord(pr[i]) - 1);
  end;
  Decrypt := Temp;
end;
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//Funcion para Obtener la version de la aplicacion
//------------------------------------------------------------------------------
function Version_Info: String;
var
  V1,       // Major Version
  V2,       // Minor Version
  V3,       // Release
  V4: Word; // Build Number
begin
  GetBuild_Info(V1, V2, V3, V4);
  Result := IntToStr(V1) + '.'
            + IntToStr(V2) + '.'
            + IntToStr(V3) + '.'
            + IntToStr(V4);
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Funcion para Crear la Estructura de un XML
//------------------------------------------------------------------------------
function MakeXmlStr (node, value: string): string;
begin
  Result := '<' + node + '>' + value + '</' + node + '>';
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Funcion para convertir un recordet en un una estructura XML
//------------------------------------------------------------------------------
function FieldsToXml (rootName: string; data: TADOQuery): string;
var
  i: Integer;
begin
  Result := '<' + rootName + '>' + sLineBreak;;
  for i := 0 to data.FieldCount - 1 do
      Result := Result + '  ' + MakeXmlStr (
                LowerCase (data.Fields[i].FieldName),
                data.Fields[i].AsString) + sLineBreak;
  Result := Result + '</' + rootName + '>' + sLineBreak;;
end;
//------------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Separa una cadea en varias segun el caracter que separa
//------------------------------------------------------------------------------

Function SepararCadena(const Cadena: string; const Delim: Char): TStringList;
begin
  Result:= TStringList.Create;
  Result.Delimiter:= Delim;
  Result.DelimitedText:= Cadena;
end;




// -----------------------------------------------------------------------------
// Formatea un numero en ######.## en texto para usar en sql
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
Function Formato(Pnumero :Double) : string;
var entero, decimal :string;
 valor :Extended;

begin
  //str(int(pnumero),entero);

    str(frac(pnumero),decimal);

    entero:=FormatFloat('0',int(pnumero))  ;
    decimal:=FormatFloat('###',frac(pnumero)*100);

    Result:=entero+'.'+ decimal;
end;

Function FormatoStr(Pnumero :string) : string;
var
i:integer;
xmonto,x : string;
begin
    xmonto:='';
    for i := 1 to  Length(pnumero) do
    BEGIN
        x:=copy(pnumero,i,1);
        if x=',' then xmonto:=xmonto+'.'
        else
        begin
          if x<>'.' then xmonto:=xmonto+x;

        END;
    END;
    Result:=xmonto;
end;




// ================================================================
// Return the three dates (Created,Modified,Accessed)
// of a given filename. Returns FALSE if file cannot
// be found or permissions denied. Results are returned
// in TdateTime VAR parameters
// ================================================================
// ================================================================
// Devuelve las tres fechas (Creaci�n, modificaci�n y �ltimo acceso)
// de un fichero que se pasa como par�metro.
// Devuelve FALSO si el fichero no se ha podido acceder, sea porque
// no existe o porque no se tienen permisos. Las fechas se devuelven
// en tres par�metros de ipo DateTime
// ================================================================
function GetFileTimes(FileName : string;
 var Created : TDateTime;
 var Modified : TDateTime;
 var Accessed : TDateTime) : boolean;
var
 FileHandle : integer;
 Retvar : boolean;
 FTimeC,FTimeA,FTimeM : TFileTime;
 LTime : TFileTime;
 STime : TSystemTime;
begin
 // Abrir el fichero
 FileHandle := FileOpen(FileName,fmShareDenyNone);
 // inicializar
 Created := 0.0;
 Modified := 0.0;
 Accessed := 0.0;

 // Ha tenido acceso al fichero?
 if FileHandle < 0 then
 RetVar := false
 else begin
 // Obtener las fechas
 RetVar := true;
 GetFileTime(FileHandle,@FTimeC,@FTimeA,@FTimeM);
 // Cerrar
 FileClose(FileHandle);
 // Creado
 FileTimeToLocalFileTime(FTimeC,LTime);
 if FileTimeToSystemTime(LTime,STime) then begin
 Created := EncodeDate(STime.wYear,STime.wMonth,STime.wDay);
 Created := Created + EncodeTime(STime.wHour,STime.wMinute,STime.wSecond,
  STime.wMilliSeconds);
 end;

 // Accedido
 FileTimeToLocalFileTime(FTimeA,LTime);
 if FileTimeToSystemTime(LTime,STime) then begin
 Accessed := EncodeDate(STime.wYear,STime.wMonth,STime.wDay);
 Accessed := Accessed + EncodeTime(STime.wHour,STime.wMinute,STime.wSecond,
  STime.wMilliSeconds);
 end;

 // Modificado
 FileTimeToLocalFileTime(FTimeM,LTime);
 if FileTimeToSystemTime(LTime,STime) then begin
 Modified := EncodeDate(STime.wYear,STime.wMonth,STime.wDay);
 Modified := Modified + EncodeTime(STime.wHour,STime.wMinute,STime.wSecond,
  STime.wMilliSeconds);
 end;
 end;

 Result := RetVar;
end;


//retorna los parametros de la tabla Z_IVAPARAMETROS
function get_BuscaParametros(pClave, pSubClave, pUsuario: String ; CnADO: TADOConnection):string;
var
    RsCnt: TADOQuery;
    Dscnt: TDataSource;
    Pr001: TParameter;
    StrSQL :String;

Begin

       StrSQL:=' SELECT VALOR ';
       StrSQL:=StrSQL +' FROM dbo.Z_IVAPARAMETROS ';
       StrSQL:=StrSQL + ' WHERE  CLAVE = :clave        ' ;
       StrSQL:=StrSQL + ' AND    SUBCLAVE = :subclave  ' ;
       StrSQL:=StrSQL + ' AND    USUARIO = :usuario    ' ;

       { Create the query. }
       RsCnt := TADOQuery.Create( RsCnt);
       RsCnt.Connection := CnADO;
       RsCnt.SQL.Add(StrSQL);

        { Update the parameter that was parsed from the SQL query }
       Pr001 := RsCnt.Parameters.ParamByName('clave');
       Pr001.DataType := ftString;
       Pr001.Value := pClave;

       Pr001 := RsCnt.Parameters.ParamByName('subclave');
       Pr001.DataType := ftString;
       Pr001.Value := pSubClave;

       Pr001 := RsCnt.Parameters.ParamByName('usuario');
       Pr001.DataType := ftString;
       Pr001.Value := pUsuario;

       RsCnt.Prepared := true;

       result:='';

       try
          RsCnt.Open;
       except
       on e: EADOError do
          begin
            MessageDlg('Error: Ejecutando el Query (function get_parametros)', mtError,
                        [mbOK], 0);
           exit;
          end;
       end;
       if not RsCnt.Eof then    Result:= RsCnt.FieldByName('valor').AsString;

       RsCnt.Close;
       RsCnt.Destroy;

End;



//------------------------------------------------------------------------------
//Procedimientos
//------------------------------------------------------------------------------

//Procedimiento para Cerrar la Conexion a la Base de datos
Procedure CloseConnection  (CnADO:TADOConnection; RsDataSet:TADOQuery);
Begin
  //Si la Conexion a la Base de datos esta Abierta
  if CnADO.Connected Then
  Begin
    with RsDataSet do begin
      if  RsDataSet.Active then
      Begin
        Close;
        Free;
      End;
      CnADO.Close;
      CnADO.Free;
    end;
  End;
End;



//Procedimiento que Cierra una aplicacion si ya se esta ejecutando
Procedure Close_EXE (Apps:String);
var
  Cerrar_Ejecutable:THandle;
  LaApps:PWideChar;
Begin
  LaApps:= PChar(Apps);
  Cerrar_Ejecutable:=FindWindow(LaApps,nil);
 // DefWindowProc(Cerrar_Ejecutable, WM_SETTEXT, 0, 0);
  if Cerrar_Ejecutable<> 0 Then ;
  Begin
    PostMessage(FindWindow(Nil, Pchar(LaApps)), WM_QUIT, 0, 0);
  end;
End;
//------------------------------------------------------------------------------

//Procedimiento Para Saber el Nombre del Pc, IP y el Usuario en Windows
Procedure ObtenerDatosPC (var Datos:TDatosPC );
const LARGO_MAXIMO = 50;
var
  buffer:Array [0..LARGO_MAXIMO+1] of char;
  Largo:Cardinal;
  PuntHost: PHostEnt;
  PuntIP: PAnsichar;
  wVersionRequested: WORD;
  wsaData: TWSAData;
begin
  Largo := LARGO_MAXIMO +1;
  Datos.Nombre := '';
  If GetComputerName (buffer, Largo) then Datos.Nombre := buffer;
  Datos.Usuario := '';
  if GetUserName(buffer, largo) then Datos.Usuario := buffer;
  wVersionRequested := MAKEWORD( 1, 1 );
  WSAStartup( wVersionRequested, wsaData );
  GetHostName( @buffer, LARGO_MAXIMO );
  PuntHost := GetHostByName( @buffer );
  PuntIP := iNet_ntoa( PInAddr( PuntHost^.h_addr_list^ )^ );
  Datos.IP := PuntIP ;
  WSACleanup;
end;
//------------------------------------------------------------------------------

//Procedimiento para escribir valores en el Registro de Windows-----------------
procedure SetRegistryData(RootKey: HKEY; Key, Value: string; RegDataType: Integer; Data: variant);
var
  Reg: TRegistry;
  s: string;
begin
  Reg := TRegistry.Create(KEY_WRITE);
  try
    Reg.RootKey := RootKey;
    if Reg.OpenKey(Key, True) then begin
      try
        if RegDataType = 0  then
          Reg.WriteString(Value, Data);
        if RegDataType = 1 then
          Reg.WriteString(Value, Data)
        else if RegDataType = 2 then
          Reg.WriteExpandString(Value, Data)
        else if RegDataType = 3 then
          Reg.WriteInteger(Value, Data)
        else if RegDataType = 4 then begin
          s := Data;
          Reg.WriteBinaryData(Value, PChar(s)^, Length(s));
        end else
          raise Exception.Create(SysErrorMessage(ERROR_CANTWRITE));
      except
        Reg.CloseKey;
        raise;
      end;
      Reg.CloseKey;
    end else
      raise Exception.Create(SysErrorMessage(GetLastError));
  finally
    Reg.Free;
  end;
end;
//------------------------------------------------------------------------------


// Procedimiento que cierra la Aplicacion
Procedure CloseApp;
Begin
  //TFrmMain.conexionADO.Close;
//    conexionADO: TADOConnection;
//    ADOQry_Clientes: TADOQuery;

  Application.Terminate;

End;
//--------------------------------------------


//Procedimiento para Borrar Archivos
//------------------------------------------------------------------------------
procedure EliminarArchivo ( sArchivo: String);
begin
 // sArchivo := ExtractFilePath( Application.ExeName ) + 'prueba.txt';
  if FileExists( sArchivo ) then
    DeleteFile( sArchivo );
end;

//------------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// Construir la informacion con la Version de la aplicacion
//------------------------------------------------------------------------------
procedure GetBuild_Info(var V1, V2, V3, V4: Word);
var
   VerInfoSize, VerValueSize, Dummy : DWORD;
   VerInfo : Pointer;
   VerValue : PVSFixedFileInfo;
begin
VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
GetMem(VerInfo, VerInfoSize);
GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
With VerValue^ do
begin
  V1 := dwFileVersionMS shr 16;
  V2 := dwFileVersionMS and $FFFF;
  V3 := dwFileVersionLS shr 16;
  V4 := dwFileVersionLS and $FFFF;
end;
FreeMem(VerInfo, VerInfoSize);
end;
//------------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Generar un arhivo texto que funcione como un archivo log
//------------------------------------------------------------------------------
procedure BitacoraTXT(Archivo,Contenido:String);
var
  Fichero: TextFile;
begin
  AssignFile(Fichero, Archivo);
  if fileexists(Archivo) then
  begin
    Append(Fichero);
  end else
  begin
    Rewrite(Fichero);
  end;
  writeLn(Fichero,Contenido);
  CloseFile(Fichero);
end;

//optiene los parametros que fueron enviados a la aplicacion, solo contempla 4 parametros
procedure GetParametros(var P1,P2,P3,P4:String) ;
 var
    i : integer;
 begin
   for i:= 0 to ParamCount do
   begin
     if i=1 then  P1:=ParamStr(i);
     if i=2 then  P2:=ParamStr(i);
     if i=3 then  P3:=ParamStr(i);
     if i=4 then  P4:=ParamStr(i);
   end;

 end;

//proceidmiento para dar formateo a los campos numericos de un query
procedure FormateaCampos(var query: TADOQuery ;  MisCamposFormateados , ptipo : string );
const formattedDate='hh:mm:ss';

var I:integer;
begin
  for i:= 0 to query.FieldCount -1 do
   if Pos(Query.Fields[i].FieldName, MisCamposFormateados) <> 0 then
   Begin
      if ptipo='N' then
        (Query.Fields[i] as TFloatField).DisplayFormat := ',#0.00' ;
      if ptipo='T' then
       (Query.Fields[i] as TDateTimeField).DisplayFormat := 'dd/mm/yyyy hh:nn:ss AM/PM' ;
   End;


end;



Procedure Save_Parametros(pClave, pSubClave, pUsuario,pParametro: String ; CnADO: TADOConnection);
var
    RsCnt: TADOQuery;
    Dscnt: TDataSource;
    Pr001: TParameter;
    StrSQL :String;

Begin

       StrSQL:=' DELETE ';
       StrSQL:=StrSQL +' FROM dbo.Z_IVAPARAMETROS ';
       StrSQL:=StrSQL + ' WHERE  CLAVE = :clave        ' ;
       StrSQL:=StrSQL + ' AND    SUBCLAVE = :subclave  ' ;
       StrSQL:=StrSQL + ' AND    USUARIO = :usuario    ' ;

       { Create the query. }
       RsCnt := TADOQuery.Create( RsCnt);
       RsCnt.Connection := CnADO;
       RsCnt.SQL.Add(StrSQL);

        { Update the parameter that was parsed from the SQL query }
       Pr001 := RsCnt.Parameters.ParamByName('clave');
       Pr001.DataType := ftString;
       Pr001.Value := pClave;

       Pr001 := RsCnt.Parameters.ParamByName('subclave');
       Pr001.DataType := ftString;
       Pr001.Value := pSubClave;

       Pr001 := RsCnt.Parameters.ParamByName('usuario');
       Pr001.DataType := ftString;
       Pr001.Value := pUsuario;

       RsCnt.Prepared := true;


       try
          RsCnt.ExecSQL;
       except
       on e: exception  do
          begin
            MessageDlg('Error: (function save_parametros)' + e.Message, mtError,
                        [mbOK], 0);
           exit;
          end;
       end;


       //insertar

       StrSQL := 'INSERT INTO dbo.Z_IVAPARAMETROS  ('    ;

       StrSQL:=StrSQL + ' CLAVE    , ' ;
       StrSQL:=StrSQL + ' SUBCLAVE , ' ;
       StrSQL:=StrSQL + ' USUARIO  , ' ;
       StrSQL:=StrSQL + ' VALOR      ' ;

       StrSQL := StrSQL + ' ) VALUES ('      ;

       StrSQL:=StrSQL + ' :clave    , ' ;
       StrSQL:=StrSQL + ' :subclave , ' ;
       StrSQL:=StrSQL + ' :usuario  , ' ;
       StrSQL:=StrSQL + ' :valor ); ' ;



       { Create the query. }
       RsCnt := TADOQuery.Create( RsCnt);
       RsCnt.Connection := CnADO;
       RsCnt.SQL.Add(StrSQL);

        { Update the parameter that was parsed from the SQL query }
       Pr001 := RsCnt.Parameters.ParamByName('clave');
       Pr001.DataType := ftString;
       Pr001.Value := pClave;

       Pr001 := RsCnt.Parameters.ParamByName('subclave');
       Pr001.DataType := ftString;
       Pr001.Value := pSubClave;

       Pr001 := RsCnt.Parameters.ParamByName('usuario');
       Pr001.DataType := ftString;
       Pr001.Value := pUsuario;

       Pr001 := RsCnt.Parameters.ParamByName('valor');
       Pr001.DataType := ftString;
       Pr001.Value := pParametro;

       RsCnt.Prepared := true;


       try
          RsCnt.ExecSQL;
       except
       on e: exception  do
          begin
            MessageDlg('Error: (function save_parametros)' + e.Message, mtError,
                        [mbOK], 0);
           exit;
          end;
       end;



       RsCnt.Close;
       RsCnt.Destroy;

End;






end.

