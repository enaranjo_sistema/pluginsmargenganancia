//******************************************************************************
// Unit que contiene los procedimientos y funciones de uso Comun Relacionados
// Con las Aplicaciones ICG
// Empresas: Consultoria & Sistemas ICG, C.A. / Geinfor Venezuela, C.A.
// Autor: Rafael Rangel
// Fecha: 14/04/2013
// Ult Modificacion: 02/05/2014
//******************************************************************************

unit UnitICG;

interface


Uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics,  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls, Vcl.Buttons, ADODB,IniFiles,  Data.DB,WinInet, WinSock,
  Registry,UnitAppsComunes;

type TICG_DataRegistry   = record
  ServerNameGeneral,
  ServerNameGestion,
  ServerNameContable,
  DBGeneral,
  DBGestion,
  DBContable,
  CajaFront,
  DescripcionFront,
  Nombreempresa:String;
  NroEmpresaGestion,
  NroEmpresaContable,
  EjercioContable,
  TipoFront,
  Version,
  Nro_Z,
  CodVendedor : Integer;
  ICGInstalado:Boolean;
  Fecha_Trabajo, Fecha_Bloqueo:TDateTime;

end;



Var

  //Conexiones a la Base de datos
  Cn001: TADOConnection;
  Cn002: TADOConnection;
  Cn003: TADOConnection;
  Rs001: TADOQuery;
  Rs002: TADOQuery;
  Rs003: TADOQuery;

  //Declaracion de Variables
  Continuar:Boolean;
  BDConectado:Boolean;
  SQL001,
  SQL002,
  SQL003:String;
  //Declaracion de Funciones
  Function Extrar_DB_Server(StrCampo:string;Devolver:Integer): string;
  //Declaracion de Procedimientos-----------------------------------------------
  //Procedimientos para tomar los valores de ICG en el Registro
  Procedure Datos_ICGRegistry (var Datos:TICG_DataRegistry;FrontICG:Integer;PCDatos:TDatosPC;NameDB,UsersDB:String);
  //Procedimientos para tomar los valores de ICG en el Registro
  Procedure Extraer_Empresas_Contables (var Datos:TICG_DataRegistry; Serie, NameDB, UsersDB:String );

  //----------------------------------------------------------------------------
  //Procedimientos para tomar los valores de la empresa de Gestion
  Procedure Extraer_Empresas_Gestion (var Datos:TICG_DataRegistry; UserDB,PasswordDB:String);

  //----------------------------------------------------------------------------
  //solo opriene registro del servidor y base de datos general de ICG
  Procedure Datos_ICGRegistry_General (var Datos:TICG_DataRegistry;FrontICG:Integer);


implementation

//******************************************************************************
// Funciones
//******************************************************************************

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------




//------------------------------------------------------------------------------



//------------------------------------------------------------------------------






//******************************************************************************
// Procedimientos
//******************************************************************************

//------------------------------------------------------------------------------
//Procedimiento para tomar los datos  de la aplicacion ICG que se este utilizando
//del registro de Windows y de los parametros de la aplicacion
//------------------------------------------------------------------------------
Procedure Datos_ICGRegistry (var Datos:TICG_DataRegistry;FrontICG:Integer;PCDatos:TDatosPC;NameDB,UsersDB:String);
Var
  Key_RegistryICG,
  Key_ServerName,
  Key_DBGeneral,
  Key_EmpresaGestion,
  StrTexto01,
  StrDelimitador:String;
  IntNumero01:Integer;
  DateFecha01:TDateTime;





Begin

  Datos.ICGInstalado:=False;
  //Se verifica si la aplicacion ICG esta Instalada en la Maquina
  Datos.ICGInstalado:=ExisteKey_Registry (HKEY_CURRENT_USER,'Software\ICG',True);
  // si Ubicamos en el registro los valores los tomamos
  if Datos.ICGInstalado then
  Begin
    //Asignamos Valores por Defecto
    Key_ServerName:='SQL_ServerName' ;
    Key_DBGeneral:='SQL_General' ;
    Key_EmpresaGestion := 'EMPGESTFRONT';

    Case FrontICG  of

      1: //FrontRest
        Begin
          //Se Define que se Buscara en el registro de Windows
          Key_RegistryICG:='\Software\ICG\FrontRest2007\';
          Datos.NroEmpresaGestion:=1;
          Key_ServerName:='ServerSQL5' ;
          Key_DBGeneral:='DataBaseSQL5' ;
          Datos.DescripcionFront:='FRONT REST';
        End;

      2 : //FronRetail
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='Software\ICG\RetailSQL2007\';
        Datos.DescripcionFront:='FRONT RETAIL';
        Datos.NroEmpresaGestion:=0;
      End;

      3 : //Front Hotel
      Begin
        //Se Define que se Buscara en el registro de Windows
      //HKEY_CURRENT_USER\Software\ICG\FrontHotel2007
        Key_RegistryICG:='\Software\ICG\FrontHotel2007\';
        Datos.DescripcionFront:='FRONT HOTEL';
        Datos.NroEmpresaGestion:=0;
      End;

      4: //Manager Advanced
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='\Software\ICG\ICGManagerSQL2007\';
        Datos.DescripcionFront:='MANAGER ADVANCED';

      End;
      5: //Manager CRM
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='\Software\ICG\ManagerCRM\';
        Datos.DescripcionFront:='MANAGER CRM';
      End;

      6: //Data Exchage
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='\Software\ICG\ICGDataExchange\';
        //Asignamos Valores por Defecto
        Key_ServerName:='SERVIDORNAME' ;
        Key_DBGeneral:='BASEDATOSGENERAL' ;
        Key_EmpresaGestion := 'CODEMPRESAGESTION';
        Datos.DescripcionFront:='DATAEXCHANGUE';
      End;

      7: //Organizer
      Begin
        //Se Define que se Buscara en el registro de Windows
        //HKEY_CURRENT_USER\Software\ICG\ICGOrganizer2007
        Key_RegistryICG:='\Software\ICG\ICGOrganizer2007\';
        Datos.DescripcionFront:='ORGANIZER';
      End;
    End;

    //Buscar en el registro
    Datos.ServerNameGeneral:= GetRegistryData(HKEY_CURRENT_USER,Key_RegistryICG, Key_ServerName);
    Datos.DBGeneral:= GetRegistryData(HKEY_CURRENT_USER,Key_RegistryICG, Key_DBGeneral);
    //'Si no hay por defecto una empresa de Gestion definida, se busca en el registro
    if Datos.NroEmpresaGestion=0 then Datos.NroEmpresaGestion:=GetRegistryData(HKEY_CURRENT_USER,Key_RegistryICG, Key_EmpresaGestion);

    //
    if FrontICG =1 then // Si es Restaurante lo tratamos diferente a las otras aplicaciones
      Begin
        Datos.ServerNameGestion:= Datos.ServerNameGeneral;
        Datos.DBGestion:= Datos.DBGeneral;
      End
    Else
      Begin


        //----------------------------------------------------------------------
        //Obtener los datos del Servidor de Gestion
        //----------------------------------------------------------------------
        Extraer_Empresas_Gestion (Datos,NameDB,UsersDB);
        //----------------------------------------------------------------------


        //Obtener los datos de la Caja
        Cn001 := TADOConnection.Create(nil);
        Rs001 := TADOQuery.Create(nil);
        BDConectado:=False;
        BDConectado:=Open_DB (Cn001, 2 , Datos.ServerNameGestion, Datos.DBGestion,'ICGAdmin', 'masterkey', '' );
        if BDConectado then
        Begin
          //Se Arma el Sql para la informacion de la caja
          SQL001:='SELECT VALOR FROM PARAMETROS WITH (NOLOCK) WHERE (CLAVE = ' + CHR(39) + 'SERIE' + Chr(39)
           + ') AND (SUBCLAVE = ' + Chr(39) + '.' + Chr(39)
           + ') AND (USUARIO = '+Chr(39)  + PcDatos.Nombre + Chr(39) + ')';
          //Se Abre el DataSet Parametros
          BDConectado:= Open_ADO_Qry(Cn001, rs001,SQL001,false);
          // Verifica si se Abrio el DataSet
          if BDConectado then
          Begin
            //
            StrTexto01:='';
            with rs001 do begin
              if not eof then
              Begin
                // Extraemos los datos del servidor y base de datos de Gestion
                StrTexto01:=Fields[0].AsString  ;
                if StrTexto01='' then   StrTexto01:='AAA';
                //Servidor y Base de datos de Gestion
                Datos.CajaFront:=StrTexto01;
              End;
            end;
          End;
        End;
        // Cerrar la Base de datos
        CloseConnection  (cn001, rs001);
        //Obtener El Z Actual de la Caja
        //Obtener los datos de la Caja
        Cn001 := TADOConnection.Create(nil);
        Rs001 := TADOQuery.Create(nil);
        BDConectado:=False;
        BDConectado:=Open_DB (Cn001, 2 , Datos.ServerNameGestion, Datos.DBGestion,'ICGAdmin', 'masterkey', '' );
        if BDConectado then
        Begin
          //Se Arma el Sql para la informacion del z actual de la caja
          SQL001:='SELECT MAX(NUMERO) + 1 FROM ARQUEOS WITH (NOLOCK) WHERE CAJA=' + Chr(39) + Datos.CajaFront + Chr(39)+ 'AND ARQUEO=' + Chr(39) + 'Z' + Chr(39) ;
         //Se Abre el DataSet Parametros
          BDConectado:= Open_ADO_Qry(Cn001, rs001,SQL001,false);
          // Verifica si se Abrio el DataSet
          if BDConectado then
          Begin
            // Leemos los valores de la tabla
            with rs001 do begin
              if not eof then
              Begin
                // Extraemos los datos del servidor y base de datos de Gestion
                IntNumero01:=Fields[0].AsInteger  ;
                if IntNumero01=0 then   IntNumero01:=1;
                //Servidor y Base de datos de Gestion
                Datos.Nro_Z:=IntNumero01;
              End;
            end;
          End;
        End;
        // Cerrar la Base de datos
        CloseConnection  (cn001, rs001);



        //Obtener la fecha de trabajo del Front
        Cn001 := TADOConnection.Create(nil);
        Rs001 := TADOQuery.Create(nil);
        BDConectado:=False;
        BDConectado:=Open_DB (Cn001, 2 , Datos.ServerNameGestion, Datos.DBGestion,'ICGAdmin', 'masterkey', '' );
        if BDConectado then
        Begin
          //Se Arma el Sql para la informacion de la fecha de trabajo
         SQL001:='SELECT CONVERT(VARCHAR(10),ISNULL(VALOR,GETDATE()), 112)      FROM PARAMETROS WITH (NOLOCK) WHERE (CLAVE = ' + CHR(39) + '$C' +Datos.CajaFront + Chr(39)
           + ') AND (SUBCLAVE = ' + Chr(39) + 'Date' + Chr(39)
           + ') AND (USUARIO = ' + Chr(39) + '.' + Chr(39) + ')';



          //Se Abre el DataSet Parametros
          //BDConectado:= Open_ADO_Qry(Cn001, rs001,SQL001,false);
          // Verifica si se Abrio el DataSet
          //if BDConectado then
          //Begin
            //
           // DateFecha01:=Date;
           // with rs001 do begin
           //   if not eof then
           //   Begin
                // Extraemos los datos del servidor y base de datos de Gestion
           //     DateFecha01:=Fields[0].AsDateTime  ;
           //     if TimeToStr(DateFecha01)='' then   DateFecha01:=Date;
                //Servidor y Base de datos de Gestion
                //Datos.Fecha_Trabajo:=DateFecha01;

            //v  End;
            //end;
          //End;
        End;
        // Cerrar la Base de datos
        CloseConnection  (cn001, rs001);


    End;
    Datos.Version:=2012;
    Datos.TipoFront:= FrontICG;
    Datos.ICGInstalado:= True;
  End
  else
      MessageDlg('No se encontro Ningun Sistema ICG Instalado ',mterror, [mbok],0);
  end   ;

//------------------------------------------------------------------------------
//busca el registro de ICG para obtener servidor y base de datos general
Procedure Datos_ICGRegistry_General (var Datos:TICG_DataRegistry;FrontICG:Integer);
Var
  Key_RegistryICG,
  Key_ServerName,
  Key_DBGeneral,
  Key_EmpresaGestion,
  StrTexto01,
  StrDelimitador:String;
  IntNumero01:Integer;
  DateFecha01:TDateTime;





Begin

  Datos.ICGInstalado:=False;
  //Se verifica si la aplicacion ICG esta Instalada en la Maquina
  Datos.ICGInstalado:=ExisteKey_Registry (HKEY_CURRENT_USER,'Software\ICG',True);
  // si Ubicamos en el registro los valores los tomamos
  if Datos.ICGInstalado then
  Begin
    //Asignamos Valores por Defecto
    Key_ServerName:='SQL_ServerName' ;
    Key_DBGeneral:='SQL_General' ;
    Key_EmpresaGestion := 'EMPGESTFRONT';

    Case FrontICG  of

      1: //FrontRest
        Begin
          //Se Define que se Buscara en el registro de Windows
          Key_RegistryICG:='\Software\ICG\FrontRest2007\';
          Datos.NroEmpresaGestion:=1;
          Key_ServerName:='ServerSQL5' ;
          Key_DBGeneral:='DataBaseSQL5' ;
          Datos.DescripcionFront:='FRONT REST';
        End;

      2 : //FronRetail
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='Software\ICG\RetailSQL2007\';
        Datos.DescripcionFront:='FRONT RETAIL';
        Datos.NroEmpresaGestion:=0;
      End;

      3 : //Front Hotel
      Begin
        //Se Define que se Buscara en el registro de Windows
      //HKEY_CURRENT_USER\Software\ICG\FrontHotel2007
        Key_RegistryICG:='\Software\ICG\FrontHotel2007\';
        Datos.DescripcionFront:='FRONT HOTEL';
        Datos.NroEmpresaGestion:=0;
      End;

      4: //Manager Advanced
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='\Software\ICG\ICGManagerSQL2007\';
        Datos.DescripcionFront:='MANAGER ADVANCED';

      End;
      5: //Manager CRM
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='\Software\ICG\ManagerCRM\';
        Datos.DescripcionFront:='MANAGER CRM';
      End;

      6: //Data Exchage
      Begin
        //Se Define que se Buscara en el registro de Windows
        Key_RegistryICG:='\Software\ICG\ICGDataExchange\';
        //Asignamos Valores por Defecto
        Key_ServerName:='SERVIDORNAME' ;
        Key_DBGeneral:='BASEDATOSGENERAL' ;
        Key_EmpresaGestion := 'CODEMPRESAGESTION';
        Datos.DescripcionFront:='DATAEXCHANGUE';
      End;

      7: //Organizer
      Begin
        //Se Define que se Buscara en el registro de Windows
        //HKEY_CURRENT_USER\Software\ICG\ICGOrganizer2007
        Key_RegistryICG:='\Software\ICG\ICGOrganizer2007\';
        Datos.DescripcionFront:='ORGANIZER';
      End;
    End;

    //Buscar en el registro
    Datos.ServerNameGeneral:= GetRegistryData(HKEY_CURRENT_USER,Key_RegistryICG, Key_ServerName);
    Datos.DBGeneral:= GetRegistryData(HKEY_CURRENT_USER,Key_RegistryICG, Key_DBGeneral);
    //'Si no hay por defecto una empresa de Gestion definida, se busca en el registro
    if Datos.NroEmpresaGestion=0 then Datos.NroEmpresaGestion:=GetRegistryData(HKEY_CURRENT_USER,Key_RegistryICG, Key_EmpresaGestion);

    //
    if FrontICG =1 then // Si es Restaurante lo tratamos diferente a las otras aplicaciones
      Begin
        Datos.ServerNameGestion:= Datos.ServerNameGeneral;
        Datos.DBGestion:= Datos.DBGeneral;
    End;
    Datos.Version:=2012;
    Datos.TipoFront:= FrontICG;
    Datos.ICGInstalado:= True;
  End
  else
      MessageDlg('No se encontro Ningun Sistema ICG Instalado ',mterror, [mbok],0);
  end   ;


//Funcion Para Extraer el Nombre del Servidor o de la Base de datos ------------
Function Extrar_DB_Server(StrCampo:string;Devolver:Integer): string ;
Var
  PosInicial,
  PosFinal,
  LargoCadena:Integer;
  Servidor:String;
  BasedeDatos:String;
  Resultado:String  ;
begin
  PosInicial:=1;
  PosFinal:=1;
  LargoCadena:=1;
  LargoCadena:=Length( StrCampo );
  PosInicial:=Pos( ':', StrCampo );
  PosFinal:=LargoCadena;
  Servidor:='';
  BasedeDatos:='';
  Servidor:=Copy(StrCampo,1,PosInicial-1);
  BasedeDatos:=Copy(StrCampo,PosInicial+1,(LargoCadena)-(PosInicial));
  Resultado:=Servidor;
  if Devolver=2 then Resultado:=BasedeDatos;
  Result:=Resultado;
end;






//------------------------------------------------------------------------------
//Funcion para Extraer los datos de las empresas Contables               NameDB:='';      UsersDB:='';
//------------------------------------------------------------------------------
Procedure Extraer_Empresas_Contables (var Datos:TICG_DataRegistry; Serie, NameDB, UsersDB:String );
Var
  StrTexto01,
  StrDelimitador:String;
  EmpContable:Integer;
  EjeContable:Integer;
Begin


  if serie<>'' then
  begin
    //Creamos los Objetos
    Cn003 := TADOConnection.Create(nil);
    Rs003 := TADOQuery.Create(nil);
    BDConectado:=False;
    BDConectado:=Open_DB (Cn003, 2 , Datos.ServerNameGestion, Datos.DBGestion,NameDB,UsersDB, '' );
    if BDConectado then
    Begin
      //Se Arma el Sql para la informacion del Servidor y empresa de Gestion
      SQL001:='SELECT CONTABILIDADB FROM SERIES WITH (NOLOCK) WHERE SERIE =' + Chr(39) + Serie + Chr(39);
      //Se Abre el DataSet de los clientes
      BDConectado:= Open_ADO_Qry(Cn003, rs003,SQL001,false);
      // Verifica si se Abrio el DataSet
      if BDConectado then
      Begin
        //
        StrTexto01:='';
        EmpContable:=1;
        EjeContable:=2012;
        with rs003 do begin
          if not eof then
          Begin
            // Extraemos los datos de la tabla de series
            StrTexto01:=Fields[0].AsString  ;
            if StrTexto01='' then   StrTexto01:='C2012010';
            //Ejercicio y Empresa Contable
            EjeContable:= StrToInt(Copy(StrTexto01,2,4));
            EmpContable:= StrToInt(Copy(StrTexto01,6,3));
            Datos.NroEmpresaContable:= EmpContable;
            Datos.EjercioContable:=EjeContable;
          End;
        end;
      End;
    End;
    // Cerrar la Base de datos
    CloseConnection  (cn003, rs003);


  end
  else
  begin

      Cn003 := TADOConnection.Create(nil);
      Rs003 := TADOQuery.Create(nil);

      BDConectado:=Open_DB (Cn003, 2 , Datos.ServerNameGeneral, Datos.DBGeneral,NameDB,UsersDB, '' );


      if BDConectado then
      Begin


         SQL001:= 'SELECT top (1) dbo.EMPRESASCONTABLES.CODIGO, dbo.EMPRESASCONTABLES.DESCRIPCION, dbo.EMPRESASCONTABLES.EJERCICIO, dbo.EMPRESASCONTABLES.PATHBD, dbo.EMPRESASCONTABLES.FECHABLOQUEO';
         SQL001:= SQL001 + ' FROM  dbo.EMPRESASCONTABLES INNER JOIN';
         SQL001:= SQL001 + ' dbo.EMPRESASCONTAUSUARIO ON dbo.EMPRESASCONTABLES.CODIGO = dbo.EMPRESASCONTAUSUARIO.CODEMPRESA AND';
         SQL001:= SQL001 + ' dbo.EMPRESASCONTABLES.EJERCICIO = dbo.EMPRESASCONTAUSUARIO.EJERCICIO';
         SQL001:= SQL001 + ' where  codempgestion=' +  InttoStr(datos.NroEmpresaGestion)  ;
         SQL001:= SQL001 + ' AND dbo.EMPRESASCONTABLES.EJERCICIO='+ IntToStr(Datos.EjercioContable) ;
         SQL001:= SQL001 + ' order by  dbo.EMPRESASCONTABLES.codigo asc , dbo.EMPRESASCONTABLES.EJERCICIO desc';

         BDConectado:= Open_ADO_Qry(Cn003, rs003,SQL001,false);
         if  rs003.eof then
         begin
             SQL001:= 'SELECT top (1) dbo.EMPRESASCONTABLES.CODIGO, dbo.EMPRESASCONTABLES.DESCRIPCION, dbo.EMPRESASCONTABLES.EJERCICIO, dbo.EMPRESASCONTABLES.PATHBD, dbo.EMPRESASCONTABLES.FECHABLOQUEO';
             SQL001:= SQL001 + ' FROM  dbo.EMPRESASCONTABLES INNER JOIN';
             SQL001:= SQL001 + ' dbo.EMPRESASCONTAUSUARIO ON dbo.EMPRESASCONTABLES.CODIGO = dbo.EMPRESASCONTAUSUARIO.CODEMPRESA AND';
             SQL001:= SQL001 + ' dbo.EMPRESASCONTABLES.EJERCICIO = dbo.EMPRESASCONTAUSUARIO.EJERCICIO';
             SQL001:= SQL001 + ' where  codempgestion=' +  InttoStr(datos.NroEmpresaGestion)  ;
             SQL001:= SQL001 + ' order by  dbo.EMPRESASCONTABLES.codigo asc , dbo.EMPRESASCONTABLES.EJERCICIO desc';

             BDConectado:= Open_ADO_Qry(Cn003, rs003,SQL001,false);

         end;

         with rs003 do begin
                StrTexto01:='';
                StrDelimitador:=':';

                if not eof then
                Begin
                  StrTexto01:=rs003.FieldByName('PATHBD').AsString  ;
                  Datos.NroEmpresaContable :=rs003.FieldByName('CODIGO').AsInteger;
                  Datos.EjercioContable :=rs003.FieldByName('EJERCICIO').AsInteger;
                  Datos.ServerNameContable :=Extrar_DB_Server(StrTexto01,1);

                  if StrTexto01='' then   StrTexto01:='Servidor:Gestion';
                  //Servidor y Base de datos de Gestion
                  Datos.ServerNameContable:=Extrar_DB_Server(StrTexto01,1);
                  Datos.DBContable:=Extrar_DB_Server(StrTexto01,2);;
                  Datos.Fecha_Bloqueo :=rs003.FieldByName('FECHABLOQUEO').AsDateTime
                End;

         end;
          // Cerrar la Base de datos
         CloseConnection  (cn003, rs003);
         exit;
    End;
  end;

  //Buscar los valores en la base de datos General
  //Para Extraer los nombres del servidor y empresa Contable
  Cn002 := TADOConnection.Create(nil);
  Rs002 := TADOQuery.Create(nil);
  BDConectado:=False;
  BDConectado:=Open_DB (Cn002, 2 , Datos.ServerNameGeneral, Datos.DBGeneral,NameDB,UsersDB, '' );
  if BDConectado then
  Begin
    //Se Arma el Sql para la informacion del Servidor y empresa de Gestion
    SQL001:='SELECT PATHBD FROM EMPRESASCONTABLES WITH (NOLOCK) WHERE CODIGO =' +  IntToStr(Datos.NroEmpresaContable)
    + ' AND EJERCICIO = ' + IntToStr(Datos.EjercioContable);
    //Se Abre el DataSet de los clientes
    BDConectado:= Open_ADO_Qry(Cn002, rs002,SQL001,false);
    // Verifica si se Abrio el DataSet
    if BDConectado then
    Begin
      //
      StrTexto01:='';
      StrDelimitador:=':';

      with rs002 do begin
        if not eof then
        Begin
          // Extraemos los datos del servidor y base de datos de Gestion
          StrTexto01:=Fields[0].AsString  ;
          if StrTexto01='' then   StrTexto01:='Servidor:Gestion';
          //Servidor y Base de datos de Gestion
          Datos.ServerNameContable:=Extrar_DB_Server(StrTexto01,1);
          Datos.DBContable:=Extrar_DB_Server(StrTexto01,2);;
        End;
      end;
    End;
  End;
  // Cerrar la Base de datos
  CloseConnection  (cn002, rs002);
End;

//------------------------------------------------------------------------------
//Funcion para Extraer los datos de las empresas de Gestion
//------------------------------------------------------------------------------
Procedure Extraer_Empresas_Gestion (var Datos:TICG_DataRegistry;UserDB,PasswordDB:String);
Var
  StrTexto01,
  StrDelimitador:String;
Begin

  //Buscar los valores en la base de datos General
  //Para Extraer los nombres del servidor y empresa de Gestion
  Cn001 := TADOConnection.Create(nil);
  Rs001 := TADOQuery.Create(nil);
  BDConectado:=False;
  BDConectado:=Open_DB (Cn001, 2 , Datos.ServerNameGeneral, Datos.DBGeneral,UserDB,PasswordDB, '' );
  if BDConectado then
  Begin
    //Se Arma el Sql para la informacion del Servidor y empresa de Gestion
    SQL001:='SELECT PATHBD,CTAEJERCICIO,TITULO FROM EMPRESAS WITH (NOLOCK) WHERE CODEMPRESA = ' + IntToStr( Datos.NroEmpresaGestion );
    //Se Abre el DataSet de los clientes
    BDConectado:= Open_ADO_Qry(Cn001, rs001,SQL001,false);
    // Verifica si se Abrio el DataSet
    if BDConectado then
    Begin
      //
      StrTexto01:='';
      StrDelimitador:=':';
      with rs001 do
      begin
        if not eof then
        Begin
          // Extraemos los datos del servidor y base de datos de Gestion
          StrTexto01:=Fields[0].AsString  ;
          if StrTexto01='' then   StrTexto01:='Servidor:Gestion';
          //Servidor y Base de datos de Gestion
          Datos.ServerNameGestion:=Extrar_DB_Server(StrTexto01,1);
          Datos.DBGestion:=Extrar_DB_Server(StrTexto01,2);
          Datos.EjercioContable := Fields[1].AsInteger;
          DATOS.Nombreempresa:=  Fields[2].AsString;
        End;
      end;
    End;
  End;
  // Cerrar la Base de datos
  CloseConnection  (cn001, rs001);
End;










end.
