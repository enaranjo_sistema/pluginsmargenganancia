//------------------------------------------------------------------------------
//Unit Principal del Plugins
//Elaborado por: Rafael Rangel
//Fecha: 2014-01-12
//Ult Modificacion:2014-01-12
//------------------------------------------------------------------------------

unit UnitMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics,  Vcl.Controls, Vcl.Forms, Vcl.Dialogs ,
  Data.DB, Data.Win.ADODB,  UnitICG,UnitAppsComunes,UnitPluggins;

type
  TFrmMain = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure ConfigApps001;
    procedure UpdateNCF;
  private
    { Private declarations }
  public
    { Public declarations }

    FSettings: TFormatSettings;

    Conectado, bGrabar :Boolean;

    Info_RegistryICG:TICG_DataRegistry;

    DatosPC: TDatosPc;

    Info_Pluggins: TICG_InfoPluggins;

  end;

var
  //Objetos
  FrmMain: TFrmMain;
  Cn001: TADOConnection;
  Cn002: TADOConnection;
  Cn003: TADOConnection;
  Rs001: TADOQuery;
  Rs002: TADOQuery;
  Rs003: TADOQuery;

  //Variables
  ServerDB,
  NameDB,
  UsersDB,
  PasswordDB,
  StrSQL,
  Mensaje,
  SerieResolucion,
  SuFactura:String;
  IdEstadoDOC,
  TipoCliente,
  NroFiscal,
  CodCliente:Integer;
  Conectado:Boolean;


implementation

{$R *.dfm}

procedure TFrmMain.FormCreate(Sender: TObject);
Var
  ArchivoXML:String;
  PCActivo:Boolean;
begin

 // Buscamos la informacion de la aplicacion en el Registro de Windows
  ConfigApps001;
  // Si la aplicacion No esta Configurada para ser ejecutara en este terminal, se cierra
  if Info_Pluggins.Activo=False then
  Begin
    CloseApp;
    Exit();
  End;

  //Obtener los datos del Pc (Nombre, IP, Usuario)
  ObtenerDatosPC(DatosPC)   ;

  // Buscamos la Informacion de ICG en el Registro
  Info_RegistryICG.NroEmpresaGestion:=Info_Pluggins.NroEmpresa;

  Datos_ICGRegistry (Info_RegistryICG,Info_Pluggins.TipoFront,DatosPC)   ;
  if Info_RegistryICG.ICGInstalado=False then     // Si existe una aplicacion ICG
  Begin
    CloseApp;
    Exit();
  End;

  ServerDB:='';
  NameDB:='';
  UsersDB:='';
  PasswordDB:='';
  ServerDB:=Info_RegistryICG.ServerNameGestion;
  NameDB:= Info_RegistryICG.DBGestion;
  UsersDB:='ICGAdmin';
  PasswordDB:='masterkey';
  PCActivo:=True;
  //PCActivo:=Ping_PC(IdIcmpClientPing,ServerDB) ;
  if (Info_Pluggins.IdPluggin) > 0 then
  Begin
    //Ubica el sql segun el tipo de front
    StrSQL:=InfoTipoFront (Info_Pluggins.TipoFront,3);
    StrSQL:=StrSQL + ' WHERE CODIGO=' +  IntToStr(Info_Pluggins.IdPluggin) ;
    //Define los Objetos ADO
    Cn001  := TADOConnection.Create(nil);
    Rs001    := TADOQuery.Create(nil);
    //Conexion a la Base de datos
    Conectado:=Open_DB (Cn001 , 2 , ServerDB, NameDB, UsersDB, PasswordDB, '' );
    //si hay conexion continue el programa
    if Conectado then
    Begin
      //Se Abre el DataSet de los clientes
      Conectado:= Open_ADO_Qry(Cn001, Rs001,StrSQL,false);
      // Verifica si se Abrio el DataSet
      if Conectado then
      Begin
        with Rs001 do begin
          if not eof then
          Begin
            // Colocamos los datos del archivo en las variables  de Memoria
            ArchivoXML:= (ExtractFilePath(Application.ExeName) + Fields[0].AsString);
          End;
        end;
      End;
    End;
    //Leer el XML
    LeerXMLPlugginFRT (ArchivoXML, Info_Documento) ;
    FrmMain.Caption:=FrmMain.Caption + ' (Conectado a  ' + Info_Documento.Server + ':' + Info_Documento.Database + ')' ;
    //Cerramos la Conexion a la Base de datos
    Rs001.Close;
    Cn001.Close;
    Rs001.Free;
    Cn001.Free;
    //Verificamos que el tipo de documento sea una factura de venta, para activar
    //El Plugings
    if Info_Documento.TipoDocumento=  'FACVENTA' then
    Begin
      //Se actualiza los NCF de acuerdo al tipo de cliente
      UpdateNCF;
    End;
  End;
  CloseApp;
  Exit();
end;


//-----------------------------------------------------------------------------
//Procedimiento que toma del registro de windos los paraemtros de la aplicacion
//-----------------------------------------------------------------------------

procedure TFrmMain.ConfigApps001;
Var
Titulo,
Llave,
Valor : string;
begin
  Titulo:='NCF en Venta';
  Llave:='Software\ICG\ICGDO_NCFVentas';
  Info_Pluggins.Configurado:=ExisteKey_Registry (HKEY_CURRENT_USER,Llave);

  // Si Existe Tomamos los datos
  if Info_Pluggins.Configurado=False then
  Begin

    // Tomamos el Valor del tipo de Front--------------------------------------
    repeat
      Valor := InputBox('Indique el Tipo de Front' ,
      'Indique El Tipo de Front:' + sLineBreak +
      '1=FrontRest' + sLineBreak +
      '2=FrontRetail' + sLineBreak  +
      '3=FrontHotel' + sLineBreak +
      '4=Manager' + sLineBreak , '2');
    until Valor <> '';
    SetRegistryData(HKEY_CURRENT_USER, Llave,'Tipofront',1,Valor);
    //--------------------------------------------------------------------------

    // Tomamos el Valor del si esta activo--------------------------------------
    repeat
      Valor := InputBox(Titulo, 'Indique Si se Activa esta Aplicación en el Terminal', 'True');
    until Valor <> '';
    SetRegistryData(HKEY_CURRENT_USER, Llave,'Activo',1,Valor);
    //--------------------------------------------------------------------------

    // Tomamos el Valor del id del Pluggin--------------------------------------
    repeat
      Valor := InputBox(Titulo, 'Indique el Id del Pluggin ', '1');
    until Valor <> '';
    SetRegistryData(HKEY_CURRENT_USER, Llave,'IdPluggin',1,Valor);
    //--------------------------------------------------------------------------

    // Tomamos el Valor del Nro de Empresa--------------------------------------
    repeat
      Valor := InputBox(Titulo, 'Indique el Nro de Empresa de Gestión ', '1');
    until Valor <> '';
    SetRegistryData(HKEY_CURRENT_USER, Llave,'NroEmpresa',1,Valor);
    //--------------------------------------------------------------------------


  End ;
  //Leemos los datos del Registro de la aplicacion
  Info_Pluggins.TipoFront:= GetRegistryData(HKEY_CURRENT_USER,Llave,'Tipofront');
  Info_Pluggins.Activo:= GetRegistryData(HKEY_CURRENT_USER,Llave,'Activo');
  Info_Pluggins.IdPluggin:= GetRegistryData(HKEY_CURRENT_USER,Llave,'IdPluggin');
  Info_Pluggins.NroEmpresa:= GetRegistryData(HKEY_CURRENT_USER,Llave,'NroEmpresa');

end;


//-----------------------------------------------------------------------------
//Procedimiento que se encarga de realizar los ajustes de lo NFC de acuerdo al
//Tipo de Cliente
//-----------------------------------------------------------------------------
procedure TFrmMain.UpdateNCF;
Var
SerieFiscal1,
SerieFiscal2: string;
LargoSerie,
NumeroFiscal,
TipoCliente:Integer;
TotalNeto:Double;
//-----------
 NumeroInicialSR,
 NumeroFinalSR,
 ContadorSR:Integer;
 SeriResol,
 NumResol:String;
//

begin
  StrSQL:='';
  StrSQL:='SELECT  FVSR.SERIEFISCAL1, FVSR.SERIEFISCAL2, FVSR.NUMEROFISCAL,' +
    ' CASE WHEN MACL.TIPO IS NULL THEN 2' +
    ' WHEN MACL.TIPO <=0  THEN 2' +
    ' ELSE MACL.TIPO END AS TIPO,' +
    ' FVCA.TOTALNETO FROM  FACTURASVENTASERIESRESOL AS FVSR ' +
    '	INNER JOIN FACTURASVENTA AS FVCA ON FVSR.NUMSERIE = FVCA.NUMSERIE ' +
    ' AND FVSR.NUMFACTURA = FVCA.NUMFACTURA AND FVSR.N = FVCA.N ' +
	  ' INNER JOIN CLIENTES AS MACL ON FVCA.CODCLIENTE = MACL.CODCLIENTE ' +
    ' WHERE (FVSR.NUMSERIE =' + Chr(39) + Info_Documento.Serie + Chr(39) + ')' +
    ' AND (FVSR.NUMFACTURA =' + IntToStr (Info_Documento.NroDocumento) + ')' +
    ' AND (FVSR.N =' + Chr(39) + Info_Documento.ModoDocumento + Chr(39) + ')' ;

  //Define los Objetos ADO
  Cn001  := TADOConnection.Create(nil);
  Rs001    := TADOQuery.Create(nil);
  //Conexion a la Base de datos
  Conectado:=Open_DB (Cn001 , 2 , Info_Documento.Server, Info_Documento.Database, Info_Documento.Users, PasswordDB, '' );
  //si hay conexion continue el programa
  if Conectado then
  Begin
    //Se Abre el DataSet de los Documentos fiscales
    Conectado:= Open_ADO_Qry(Cn001, Rs001,StrSQL,false);
    // Verifica si se Abrio el DataSet
    if Conectado then
    Begin
      with Rs001 do
      begin
        if not eof then
        Begin
          SerieFiscal1:= Fields[0].AsString;
          SerieFiscal2:= Fields[1].AsString;
          NumeroFiscal:= Fields[2].AsInteger;
          TipoCliente:=  Fields[3].AsInteger;
          TotalNeto:=  Fields[4].AsFloat;
        End;
      end;
      //Cerramos El RecordSet
      Rs001.Close;
      //Verificamos si el tipo de cliente o documento requiere realizar el cambio de NCF
      if (TipoCliente=1)or (TipoCliente=14) or (TipoCliente=15) Or (TotalNeto<0) then
      Begin
        //Restamos el contador, para volverlo al numero anterior, ya que se va
        //a Cambiar el numero de NCF de acuerdo al tipo de clientes
        NumeroFiscal:=NumeroFiscal-1;
        if NumeroFiscal < 0  then NumeroFiscal:=0;
        //Ajustamos el tipo de cliente a la manera como se configuro las series de resulucion
        //Si es una nota de credito se considera el NCF para Notas de Credito
        if TotalNeto <=0  then TipoCliente:=4;
        if TipoCliente <= 9  then TipoCliente:=TipoCliente*10;
        //Se Genera el SQL para devolver el contador de este NCF
        StrSQL:='';
        StrSQL:='UPDATE SERIESRESOLUCION SET  CONTADOR=' + IntToStr(NumeroFiscal) +
          ' WHERE (SERIERESOL=' + Chr(39) +  SerieFiscal1 + Chr(39) + ')' +
          ' AND (NUMRESOL=' + Chr(39) +  SerieFiscal2 + Chr(39) + ')'  ;
        //Se ejecuta el SQL para  devolver el contador de este NCF
        Conectado:= DB_Function(Cn001,StrSQL,4);


        // Si los tipos de cliente son 14 o 15 se debe realizar un trato especial a la serie de resolucion
        if ((TipoCliente=14) or (TipoCliente=15))   then
        Begin
          LargoSerie:=Length(SerieFiscal1) ;
          if LargoSerie >=10 then  SerieFiscal1:=Copy(SerieFiscal1,1,9)

        End;



        //Se prepara el SQL para Buscar el NCF que le corresponde
        NumeroFiscal:=0;

        StrSQL:='';
        StrSQL:='SELECT TOP (1) NUMEROINICIAL, NUMEROFINAL, CONTADOR,SERIERESOL,NUMRESOL ' +
          ' FROM SERIESRESOLUCION WHERE (SERIERESOL = ' + Chr(39) +  SerieFiscal1 + Chr(39) + ')' +
          ' AND (SUBSTRING(NUMRESOL, 1, 2) = '+ Chr(39) + IntToSTR(TipoCliente) + Chr(39) + ')' +
          ' ORDER BY NUMRESOL DESC';
        //Se Busca el NCF que le corresponde
        Conectado:= Open_ADO_Qry(Cn001, Rs001,StrSQL,false);
        // Verifica si se Abrio el DataSet
        if Conectado then
        Begin
          with Rs001 do
          begin
            if not eof then
            Begin
              NumeroInicialSR:= Fields[0].AsInteger;
              NumeroFinalSR:= Fields[1].AsInteger;
              ContadorSR:= Fields[2].AsInteger;
              SeriResol:= Fields[3].AsString;
              NumResol:= Fields[4].AsString;
            End;
          end;
          //Se prepara el NCF que le corresponde
          if ContadorSR=0 then ContadorSR:=  NumeroInicialSR-1;
          ContadorSR:= ContadorSR+1;
          //Se ejecuta el SQL para  Actualizar el contador del NCF que Corresponde
          StrSQL:='';
          StrSQL:='UPDATE SERIESRESOLUCION SET  CONTADOR=' + IntToStr(ContadorSR) +
          ' WHERE (SERIERESOL=' + Chr(39) +  SeriResol + Chr(39) + ')' +
          ' AND (NUMRESOL=' + Chr(39) +  NumResol + Chr(39) + ')'  ;
          //Se ejecuta el SQL para  devolver el contador de este NCF
          Conectado:= DB_Function(Cn001,StrSQL,4);
          //Se ejecuta el SQL para  Actualizar en la Tabla de FacturasVentaSeriResolucion
          //El NCF que Corresponde
          StrSQL:='';
          StrSQL:='UPDATE FACTURASVENTASERIESRESOL SET SERIEFISCAL2=' + Chr(39) + NumResol + Chr(39) + ',' +
            ' NUMEROFISCAL=' + IntToStr(ContadorSR) +   ',' +
            ' SERIEFISCAL1=' + Chr(39) + SerieFiscal1 + Chr(39) +
            ' WHERE (NUMSERIE =' + Chr(39) + Info_Documento.Serie + Chr(39) + ')' +
            ' AND (NUMFACTURA =' + IntToStr (Info_Documento.NroDocumento) + ')' +
            ' AND (N =' + Chr(39) + Info_Documento.ModoDocumento + Chr(39) + ')' ;
          Conectado:= DB_Function(Cn001,StrSQL,4);

        End;
      End;
    End;
    // Cerrar la Conexion a la Base de datos
    CloseConnection  (cn001, rs001);
  End;
end;

end.
