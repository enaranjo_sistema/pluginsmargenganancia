//******************************************************************************
// Unit que contiene los procedimientos y rutinas necesarios para poder Leer y
// Escribir en Archivos .INI
// Empresas: Consultoria & Sistemas ICG, C.A. / Geinfor Venezuela, C.A.
// Autor: Rafael Rangel
// Fecha: 31/03/2013
// Ult Modificacion: 31/03/2013
//******************************************************************************


unit UnitINI;

interface

Uses
IniFiles;

var
  IniFile : TIniFile;
  // Funciones
  function ReadCadINI (clave, cadena,ArchivoINI : string; defecto : string) : string;
  function ReadBoolINI (clave, cadena,ArchivoINI : string; defecto : boolean) : boolean;
  function ReadEntINI (clave, cadena,ArchivoINI : string; defecto : integer) : integer;
  function ReadFechaINI (clave, cadena,ArchivoINI : string; defecto : tdatetime) : tdatetime;
  function ReadFloatINI (clave, cadena,ArchivoINI : string; defecto : double) : double;


{  function SectionExists(const Section: string): Boolean;
  procedure EraseSection(const Section: string);
  procedure DeleteKey(const Section, Ident: String);
  procedure UpdateFile;
  function ValueExists(const Section, Ident: string): Boolean;}



  // Procedimientos
  procedure WriteBoolINI (clave, cadena,ArchivoINI : string; valor : boolean);
  procedure WriteEntINI (clave, cadena,ArchivoINI : string; valor : integer);
  procedure WriteCadINI (clave, cadena, ArchivoINI,valor : string);
  procedure esFloatINI (clave, cadena,ArchivoINI : string; valor : double);
  procedure esFechaINI (clave, cadena,ArchivoINI : string; defecto : tdatetime);


implementation


//******************************************************************************
//Lectura
//Lee una cadena de texto de un INI
function ReadCadINI (clave, cadena,ArchivoINI : string; defecto : string) : string;
begin
  with tinifile.create (ArchivoINI) do
  try
    result := readString (clave, cadena, defecto);
  finally
    free;
  end;
end;

//Lee un booleano de un INI
function ReadBoolINI (clave, cadena,ArchivoINI : string; defecto : boolean) : boolean;
begin
  with tinifile.create (ArchivoINI) do
  try
    result := readbool (clave, cadena,defecto);
  finally
    free;
  end;
end;
//Lee un entero de un INI
function ReadEntINI (clave, cadena,ArchivoINI : string; defecto : integer) : integer;
begin
  with tinifile.create (ArchivoINI) do
  try
    result := readInteger (clave, cadena, defecto);
  finally
    free;
  end;
end;

//lee una fecha de un INI
function ReadFechaINI (clave, cadena,ArchivoINI : string; defecto : tdatetime) : tdatetime;
begin
  with tinifile.create (ArchivoINI) do
  try
    result := ReadDate (clave, cadena, defecto);
  finally
    free;
  end;
end;

//lee un valor num�rico de un INI
function ReadFloatINI (clave, cadena,ArchivoINI : string; defecto : double) : double;
begin
  with tinifile.create (ArchivoINI) do
  try
    result := readfloat (clave, cadena, defecto);
  finally
    free;
  end;
end;
//------------------------------------------------------------------------------


//******************************************************************************
//---------------------Escritura en Archivos INI -------------------------------
//escribe un Booleano en un INI
procedure WriteBoolINI (clave, cadena,ArchivoINI : string; valor : boolean);
begin
  with tinifile.create (ArchivoINI) do
  try
    writeBool (clave, cadena, valor);
  finally
    free;
  end;
end;


//Escribe un entero en un INI
procedure WriteEntINI (clave, cadena,ArchivoINI : string; valor : integer);
begin
  with tinifile.create (ArchivoINI) do
  try
    writeInteger (clave, cadena, valor);
  finally
    free;
  end;
end;


//Escribe una cadena de texto en un INI
procedure WriteCadINI (clave, cadena, ArchivoINI,valor : string);
begin
 with tinifile.create (ArchivoINI) do
  try
    writeString (clave, cadena, valor);
  finally
    free;
  end;
end;


//Escribe un n�mero en un INI
procedure esFloatINI (clave, cadena,ArchivoINI : string; valor : double);
begin
  with tinifile.create (ArchivoINI) do
  try
    WriteFloat (clave, cadena, valor);
  finally
    free;
  end;
end;


//Escribe una fecha en un INI
procedure esFechaINI (clave, cadena,ArchivoINI : string; defecto : tdatetime);
begin
  with tinifile.create (ArchivoINI) do
  try
    WriteDate (clave, cadena, defecto);
  finally
    free;
 end;
end;

//------------------------------------------------------------------------------


end.

